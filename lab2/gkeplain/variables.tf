variable "name" { default = "cluster-1" }
variable "zone" { default = "us-central1-f" }
variable "network" { default = "default" }
variable "min_master_version" { default = "1.9" }
variable "machine_type" { default = "n1-standard-2" }
variable "image_type" { default = "ubuntu" }
